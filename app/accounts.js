"use strict";
const Mixer = require('beam-client-node'),
    fs = require('fs'),
    path = require('path'),
    util = require(path.join(__dirname, 'Utils'));
let streamClient = new Mixer.Client(new Mixer.DefaultRequestRunner()),
    botClient = new Mixer.Client(new Mixer.DefaultRequestRunner());
let streamerAcc,
    botAcc,
    cid;
var log = function(msg){
    console.log(`
    ----------------------------------------------------
    [SmartBot] ${msg}
    ----------------------------------------------------`);
};
var info = function (msg) {
    console.log(`
    ----------------------------------------------------
    [SmartBot] ${msg}
    ----------------------------------------------------`);
};
var error = function (msg) {
    console.log(`
    ----------------------------------------------------
    [RipBot] ${msg}
    ----------------------------------------------------`);
};

module.exports.log = function (msg) {
    return log(msg);
};
module.exports.info = function (msg) {
    return info(msg);
};
module.exports.error = function (msg) {
    return error(msg);
};
var tokenStreamer, tokenBot, config;
try{
    config = JSON.parse(fs.readFileSync(path.join(__dirname,'../text/authKeys.json'), 'utf-8'));
    tokenStreamer = config.streamer;
    tokenBot = config.bot;

    /*
    MAKE SURE FILES EXIST!
     */
    files().then(()=> {tokenNull();}).catch(err=>{error(err);});


}catch (e){
    error(`Error starting bot ${e}`);
}
let ca,cs;
cs = require(path.join(__dirname,'chat/chatSocket'));
ca = require(path.join(__dirname,'./carina'));

function files() {

    return new Promise(async function (res,err) {
        try{
            if(!fs.existsSync(path.join(__dirname,'../text/chatSettings.json')) || !fs.existsSync(path.join(__dirname,'../text/queue.json')) || !fs.existsSync(path.join(__dirname,'../text/commands.json')) || !fs.existsSync(path.join(__dirname,'../text/users.json')) || !fs.existsSync(path.join(__dirname,'../text/events.json'))){
                if(!fs.existsSync('../text/chatSettings.json'))
                    fs.writeFileSync(path.join(__dirname,'../text/chatSettings.json'), JSON.stringify(util.defaultJson("chat")));

                if(!fs.existsSync(path.join(__dirname,'../text/queue.json')))
                    fs.writeFileSync(path.join(__dirname,'../text/queue.json'), JSON.stringify(util.defaultJson("queue")));

                if(!fs.existsSync(path.join(__dirname,'../text/commands.json')))
                    fs.writeFileSync(path.join(__dirname,'../text/commands.json'), JSON.stringify(util.defaultJson("cmds")));

                if(!fs.existsSync(path.join(__dirname,'../text/users.json')))
                    fs.writeFileSync(path.join(__dirname,'../text/users.json'), JSON.stringify(util.defaultJson("users")));

                if(!fs.existsSync(path.join(__dirname,'../text/events.json')))
                    fs.writeFileSync(path.join(__dirname,'../text/events.json'), JSON.stringify(util.defaultJson("events")));

                if(fs.existsSync(path.join(__dirname,'../text/chatSettings.json')) && fs.existsSync(path.join(__dirname,'../text/queue.json')) && fs.existsSync(path.join(__dirname,'../text/commands.json')) && fs.existsSync(path.join(__dirname,'../text/users.json')) && fs.existsSync(path.join(__dirname,'../text/events.json'))){
                    res(true);
                }
            }else{
                res(true);
            }



        }catch (e){
            err(e);
        }
    })
}
module.exports.createFiles = function(){
    return new Promise(res=>{
        files().then(x=>{res(x);});
    });

};

async function tokenNull(skip) {
    if(skip !== (null || undefined)){
        window.alert("Would you like to have a separate bot account?");
    }else{
        while (tokenStreamer == null){
            setTimeout(getConf, 2000);
        }
        if(tokenStreamer !== null){
            if(tokenBot === null || tokenBot === tokenStreamer){
                window.alert("Would you like to have a separate bot account?");

            }else{
                main();
            }
        }
    }


}

async function getConf() {
    try {
        config = JSON.parse(fs.readFileSync(path.join(__dirname,'../text/authKeys.json'), 'utf-8'));
        tokenStreamer = config.streamer;
        tokenBot = config.bot;
        if(tokenBot === null)
            tokenBot = tokenStreamer;
    }catch (e) {

    }

}

async function main(load) {
    try{
        await getConf();
        console.log(tokenStreamer + " " + tokenBot);
        var mainErr = false;
        info('SmartBot is attempting to start (v 0.6-Alpha)');
        info('Attempting to connect to event system...');
        streamClient.use(new Mixer.OAuthProvider(streamClient, {
            tokens: {
                access: tokenStreamer,
                expires: Date.now() + (365 * 24 * 60 * 60 * 1000)
            },
        }));

        botClient.use(new Mixer.OAuthProvider(botClient, {
            tokens: {
                access: tokenBot,
                expires: Date.now() + (365 * 24 * 60 * 60 * 1000)
            }
        }));

        botClient.request('GET', `users/current`)
            .then(async res => {
                botAcc = res.body;
                if(res.body.statusCode === 401){
                    tokenNull(true);
                    console.log("Bot error go to token null?");
                }else{
                    streamClient.request('GET', `users/current`)
                        .then(async response => {
                            streamerAcc = response.body;
                            if(streamerAcc.user_name === botAcc.user_name && load !== true){
                                tokenNull(true);
                                console.log("Bot error go to token null?");
                                mainErr = true;
                                return false;
                            }else{
                                cid = response.body.channel.id;
                                ca.update(cid);
                                ca.follow(cid);
                                ca.sub(cid);
                                ca.host(cid);
                                util.getViewers();
                                return new Mixer.ChatService(botClient).join(cid);
                            }
                        })
                        .then(response => {
                            info('Attempting to connect to the chat...');
                            if(!mainErr){
                                const body = response.body;
                                return cs.createChatSocket(botAcc.id, cid, body.endpoints, body.authkey);
                            }else{
                                info("Chat connection failed due to error.")
                            }

                        }).catch(err=>{mainErr = true; error(err); process.exit(404);});
                }
            }).catch(err=>{error(err);});

    }catch(e){
        error(`Error starting bot, please contact Unsmart with the following: \n${e}`);
    }

}
module.exports.mainRunner = function () {
    main();
};
module.exports.client = function () {
  return streamClient;
};
module.exports.id = function () {
    return cid;
};

/*
HANDLE EXITING/CRASHING/ETC.
 */
let clean = false;
process.stdin.resume();

function exitHandler() {
    if(clean){
        process.exit(0);
    }else{
        try{
            info(`Getting the hours of all users before closing...`);
            util.getHours(undefined, true).then(()=>{
                appendChat("BOT", `Thanks for using SmartBot! If you have any questions or want to put in a request contact Unsmart at harpcoding@gmail.com Thanks!`, "gold");
                clean = true;
                process.exit(0);
            });

        }catch(e){

        }
    }

}

//do something when app is closing
process.on('exit', exitHandler.bind(null));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null));
process.on('SIGUSR2', exitHandler.bind(null));

//catches uncaught exceptions
process.on('uncaughtException', function (e) {
    error(`Uncaught exception ---- \n${e.stack}`);
    process.exit(404);
});


function appendChat(username, message, color) {
    var chatMsg = '';
    var append = document.createElement("span");
    append.id = "chat-container";
    var userDiv = document.createElement("div");
    var userText = document.createTextNode(username);
    userDiv.appendChild(userText);
    userDiv.id = "username";
    userDiv.style.color = color;
    append.appendChild(userDiv);

    message.forEach(function (msgPart) {
        if(msgPart.type === "emoticon"){
            if(chatMsg !== ('' || ' ')) {
                var doc = document.createElement("div");
                doc.id = "message";
                doc.appendChild(document.createTextNode(chatMsg.trim() + ' '));
                append.appendChild(doc);
            }
            chatMsg = ' ';
            var url;
            if(msgPart.source === "external")
                url = msgPart.pack;
            else
                url = "https://mixer.com/_latest/assets/emoticons/" + msgPart.pack + ".png";

            var emote = document.createElement("span");
            emote.style.cssText = `height: 24px; width: 24px; background-repeat: no-repeat; display: inline-block; background-image: url(${url}); background-position: -${msgPart.coords.x}px -${msgPart.coords.y}px;`;
            append.appendChild(emote);
        }else{
            chatMsg += msgPart.text;
        }
    });
    if(chatMsg !== ('' || ' ')){
        var doc = document.createElement("div");
        doc.id = "message";
        doc.appendChild(document.createTextNode(chatMsg.trim()));
        append.appendChild(doc);
    }

    window.appendToChat(append);
    document.getElementById("chat").appendChild(append);
}
module.exports.appendChat = (username, message, color) =>{
    return appendChat(username, message, color);
};


module.exports.say = (msg) =>{
    cs.chat(msg);
};