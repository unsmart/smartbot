var hours;
const fs = require("fs"), path = require('path'), ac = require(path.join(__dirname,'accounts')), cs = require(path.join(__dirname,'chat/chatSocket'));


/*
SET DEFAULT JSON
 */
const queue = {
    "queue": {
        "on": false,
        "list": []
    }
};
const cmds = {
    "commands":{}
};
const events = {
    "sessionEvents":{
        "follows":[],
        "subscribes":[],
        "hosts":[]
    }
};
const users = {
    "users":{}
};
const chat = {
    "blacklist": [],
    "repeatMax": null
};
module.exports.defaultJson = function (str) {
    if(str === "cmds")
        return cmds;
    else if(str === "events")
        return events;
    else if(str === "users")
        return users;
    else if(str === "queue")
        return queue;
    else
        return chat;
};

/*
SET TITLE
 */
async function setTitle(msg) {
    try{
        let newTitle;
        msg.trim();
        ac.client().request(`PATCH`, `channels/${ac.id()}`,{body:{name: msg}})
            .then(res=>{
                newTitle = res.body.name;
                cs.chat(`The title has been set to '${newTitle}'`);
            });
    }catch (e) {
        ac.error(`Error setting title, send the following to Unsmart: \n${e}`);
    }
}
module.exports.titleSet = function (newTitle) {
    return setTitle(newTitle);
};

/*
SET GAME
 */
async function setGame(msg) {
    let gameName,gameID;
    ac.client().request(`GET`, `types?query=${msg}`).then(res=> {gameID = res.body[0].id; gameName = res.body[0].name;})
        .then(() => {ac.client().request('PATCH', `channels/${ac.id()}`,{body:{typeId: gameID}}); cs.chat(`Closest search result was '${gameName}'. The game has now been set!`); console.log('Game name:', gameName);});
}
module.exports.gameSet = function(newGame){
    return setGame(newGame);
};
async function getGame() {
    return new Promise(function (resolve, reject) {
        ac.client().request(`GET`, `https://mixer.com/api/v1/channels/${ac.id()}?fields=type`).then( x =>{
            resolve(x.type.name);
        }).catch(err=>{
            reject(err);
        });
    });

}
module.exports.getGame = function(){
    return new Promise(resolve => {
        getGame().then(x=>{resolve(x);});
    });
};
/*
SET RATING
 */
function setRating(msg){
    ac.client().request(`PATCH`, `channels/${ac.id()}`, {
        body:{
            audience: msg
        }
    }).then(res =>{
        cs.chat(`New rating set to '${res.body.audience}'`);
    });
}
module.exports.ratingSet = function (newRating) {
    return setRating(newRating);
};

/*
GET VIEWERS
 */
async function getViewers(users){
    return new Promise(async function (resolve, err) {
        try {
            getFile('users').then(async main => {
                if (typeof users === "undefined") {
                    var viewerList = [];
                    var run = (page) => {
                        ac.client().request(`GET`, `chats/${ac.id()}/users`, {qs: {page}}).then(async res => {
                            if (res.body.length === 0) {
                                if (await isLive()) {
                                    viewerList.forEach(async function (user) {
                                        if (main.users.hasOwnProperty(user)) {
                                            if (main.users[user].joinTime === null) {
                                                main.users[user].joinTime = new Date().toISOString();
                                                ac.log(`${user} was given a join time. [From getViewers()] ---- viewersList`);
                                                fs.writeFileSync(path.join(__dirname,`../text/users.json`), JSON.stringify(main, null, 2), {mode: 0o777});
                                            }
                                        } else {
                                            newUser(user);
                                        }
                                    });
                                } else {
                                    viewerList.forEach(async function (user) {
                                        if (main.users.hasOwnProperty(user)) {
                                            if ((typeof main.users[user].joinTime === 'string'))
                                                getHours(user);
                                        } else {
                                            newUser(user);
                                        }
                                    });
                                }
                            } else {
                                var doneWith = 0;
                                res.body.forEach(function (item) {
                                    viewerList.push(item.userName);
                                    doneWith++;
                                    if (doneWith === res.body.length)
                                        return run(page + 1);
                                });

                            }

                        });
                    };
                    return run(0);
                } else {
                    if (await isLive()) {
                        if (main.users.hasOwnProperty(users)) {
                            if (main.users[users].joinTime === null) {
                                main.users[users].joinTime = new Date().toISOString();
                                ac.log(`${users} was given a join time. [From getViewers(user)]`);
                                fs.writeFileSync(path.join(__dirname,`../text/users.json`), JSON.stringify(main, null, 2), {mode: 0o777});
                            }else{
                                //ac.log(users + ' already has a join time!');
                            }
                        } else {
                            newUser(users);
                        }
                    } else {
                        if (main.users.hasOwnProperty(users)) {
                            if ((typeof main.users[users].joinTime === 'string'))
                                getHours(users);
                        } else {
                            newUser(users);
                        }
                    }
                }
            }).then(()=>{resolve(true);});

        } catch (e) {
            ac.error(`Error getting viewers and assigning join time or giving hours, send the following to Unsmart: \n${e}`);
            err(e);
        }
    });

}
module.exports.getViewers = function(user){
    return getViewers(user);
};

/*
GET FOLLOWERS
 */
function getFollowers(users){
    if(typeof users === "undefined"){
        var followers = [], user;
        const run = (page) =>{
            user = undefined;
            ac.client().request(`GET`, `channels/${ac.id()}/follow`,{qs:{page}}).then(res=>{
                res.body.forEach(function (channel) {
                    user = channel.username;
                    followers.push(user);
                });
                if(res.body.length === 0)
                    console.log('Current list of followers', followers);
                else
                    return run(page+1);
            });

        };
        return run(0);

    }else{
        ac.client().request(`GET`, `channels/${ac.id()}/follow`).then(res=>{
            res.body.forEach(function (channel) {
                var userx = channel.username === users;
                if(userx){
                    return true;
                }
            })
        });
    }

}
module.exports.getFollowers = function (user) {
    return getFollowers(user);
};

/*
GET HOURS
 */
async function getHours(userMain, closing) {
    if(!(closing === undefined)){
        var close = true;
    }
    return new Promise(async function (resolve,reject) {
        try {
            var given = [];
            getFile('users').then(async main =>{
                if (typeof userMain === "undefined") {
                    for (let user in main.users) {
                        if (!(main.users[user].joinTime == null)) {
                            hours = roundUp(main.users[user].hours) + dateConvert(main.users[user].joinTime,true);
                            main.users[user].hours = hours;
                            if(!close){
                                isLive().then(live=>{
                                    main.users[user].joinTime = live ? new Date().toISOString() : null;
                                });
                            }else{
                                main.users[user].joinTime = null;
                            }
                            given.push(user);
                        }
                    }
                    if(given.length >= 1){
                        fs.writeFile(path.join(__dirname,'../text/users.json'), JSON.stringify(main,null,2), ()=>{
                            if(given.length === 1){
                                ac.log(given.toString() + ' has been given hours.');
                            }else if(given.length === 2){
                                ac.log(given.toString() + ' have both been given hours.');
                            }else{
                                ac.log(given.toString() + ' have all been given hours.');
                            }

                            resolve(given.length, given);
                        });
                    }else{
                        ac.log('No users were given hours. No one had been given a join time, if you believe this to be an error please contact Unsmart at harpcoding@gmail.com');
                        resolve(0);
                    }


                } else {
                    if(main.users.hasOwnProperty(userMain)){
                        if (typeof main.users[userMain].joinTime === 'string') {
                            hours = main.users[userMain].hours + dateConvert(main.users[userMain].joinTime, true);
                            main.users[userMain].hours = roundUp(hours, 2);
                            isLive().then(live=>{
                                main.users[userMain].joinTime = live ? new Date().toISOString() : null;
                                fs.writeFile(path.join(__dirname,'../text/users.json'), JSON.stringify(main,null,2), ()=>{
                                    resolve(true);
                                });
                            });
                            ac.log(userMain + ' given hours.');
                        } else {
                            resolve(true);
                        }
                    }else{
                        newUser(userMain);
                    }

                }
            });


        } catch (e) {
            ac.error(`Error getting hours, send the following to Unsmart: \n${e}`);
            reject(e);
        }
    });

}
module.exports.getHours = function (user, closing) {
    return new Promise(resolve => {
        getHours(user, closing).then(()=>{
            resolve(true);
        });
    });
};

/*
NEW USER
 */
async function newUser(user){
    return new Promise(function (resolve, reject) {
        try{
            getFile('users').then(async main=>{
                main.users[user] = JSON.parse(`{"points": 0, "hours": 0, "following": false, "lastMsg": null, "joinTime":null}`);
                fs.writeFileSync(path.join(__dirname,'../text/users.json'), JSON.stringify(main,null,2), {mode: 0o777});
                getFile('users').then(async x=>{
                    if(x.users.hasOwnProperty(user)){
                        getViewers(user).then(()=>{
                            resolve(true);
                        });

                    }else{
                        newUser(user).then(()=>{
                            resolve(true);
                        });
                    }
                });
            });


        }catch (e){
            ac.error(`Error creating new user, send the following to Unsmart: \n${e}`);
            reject(e);
        }

    });


}
module.exports.newUser = (user) => {
    return new Promise(function (resolve) {
        newUser(user).then(x=>{
            resolve(x);
        })
    });
};


/*
GET FILES
 */
function getFile(str){
    var x = new Promise(function (res,rej) {
        try{
            ac.createFiles();
            var file = JSON.parse(fs.readFileSync(path.join(__dirname,`../text/${str}.json`), 'utf8'));
            res(file);
        }catch (e){
            ac.error(`Error getting file, send the following to Unsmart (${str}): \n${e}`);
            rej(e);
        }
    });


    return new Promise(resolve => {
        x.then(res =>{
            resolve(res);
        });
    });

}
module.exports.getFile = function (str) {
    return new Promise(resolve => {
        getFile(str).then(res =>{
            resolve(res);
        })
    });
};

/*
ROUND
 */
function roundUp(num, precision) {
    precision = Math.pow(10, precision);
    return Math.floor(num * precision) / precision;
}
module.exports.round = function (num,perc) {
    return roundUp(num,perc);
};

/*
TIME ONLINE
 */
function onlineTime(cb){
    try {
        ac.client().request('GET', `channels/${ac.id()}/broadcast`).then(async res => {
            let iso = res.body.startedAt;
            cb(!!iso ? await dateConvert(iso) : '');
        });
    }catch(e){
        ac.error(`Error getting online time, send the following to Unsmart: \n${e}`);
    }
}
function getStreaming(){
    return new Promise(function (resolve, reject) {
        try{
            onlineTime(function (result) {
                resolve(result);
            });
        }catch (ex){
            reject(ex);
        }
    });
}
module.exports.getStreamTime = function () {
    return getStreaming();
};


/*
IS STREAM LIVE
 */
function isLive(){
    return new Promise(function(res){
        getStreaming().then(function (live) {
            res(live !== '');
        });
    });

}
module.exports.isLive = function () {
    return isLive();
};





function dateConvert(iso, bool){
    var time = Math.abs((new Date(iso)) - (new Date()));
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~(time % 60);
    if(bool === undefined){
        bool = false;
    }
    return bool ? time / 3600 :  (hrs === 0 ? "" : hrs + " hours") + (mins === 0 ? "" : " " + mins + " minutes") + (secs === 0 ? "" : " " + secs + " seconds");
}

