"use strict";

const Mixer = require('beam-client-node'),
    ws = require('ws'),
    path = require('path'),
    util = require(path.join(__dirname,'../Utils')),
    fs = require("fs"),
    ac = require(path.join(__dirname,'../accounts'));

let reply, socket, say;
var tryCount = 5;
/*
FILES
 */
var queueF = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/queue.json'))),
    replySettings = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/textSettings.json'))),
    custom = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/commands.json'))),
    chatSettings = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/chatSettings.json'))),
    chatLogs = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/chatLogs.json')));


/*
VARS
 */
let queueOn = queueF.queue.on,
    queueMode = queueF.queue.mode,
    prefix = custom.commands.prefix.trim().toLowerCase()[0],
    repeatMax = chatSettings.repeatMax;

function cs(userId, channelId, endpoints, authkey) {
    // noinspection JSCheckFunctionSignatures
    socket = new Mixer.Socket(ws, endpoints).boot();

    socket.on('UserJoin', data => {
        ac.info(`${data.username} has joined.`);
        util.getViewers(data.username);
    });

    socket.on('UserLeave', data => {
        let user = data.username;
        ac.info(`${user} has left.`);
        util.getHours(user.toString());
    });

    socket.on('ClearMessages', function (data) {
        ac.log(`Chat cleared by ${data.clearer.user_name}.`);
    });

    socket.on('DeleteMessage', data => {
        var user = data.moderator.user_name;
        let msgDel = 'msg unknown probably a command', pos = 0;
        chatLogs = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/chatLogs.json')));
        chatLogs.Logs.some(x =>{
            if(x.id === data.id){
                msgDel = x.msg;
                chatLogs.Logs.splice(pos,1);
                fs.writeFileSync(path.join(__dirname,'../../text/chatLogs.json'), JSON.stringify(chatLogs,null,2));
            }
            pos++;
        });
        ac.log("Message removed by " + user + "! (msg removed -> \"" + msgDel + "\")");
    });

    socket.on('ChatMessage', data => {

        let admin = data.user_id === 755643,
            superUser = (~data.user_roles.toString().toLowerCase().indexOf(('mod') || ('owner')) || admin),
            msg,
            cmd = data.message.message[0].data.toLowerCase().split(' ')[0],
            chat = data.message.message[0].data.replace(prefix, ''),
            user = data.user_name,
            target = null;

        data.message.message.some(x => {
            if (x.type === "tag") {
                target = x.username;
                return true;
            }
        });

        util.getViewers(user);

        /*
         * COMMANDS
         */

        if (cmd.startsWith(`${prefix}`) || (admin && cmd.startsWith('!'))) {
            cmd = cmd.substring(1);
            /*
            ADMIN COMMANDS
             */
            if (admin && cmd === ('admin')) {
                try {
                    if (~chat.indexOf('raid')) {
                        if (~chat.indexOf('rl'))
                            socket.call('msg', [`Hi! Welcome to the HypeZone! You're among the closest on Mixer to hitting the ball thing with your little car thingy. Everyone is here to cheer you on in the pursuit of your stuff. Have fun or whatever.`]);
                        else
                            socket.call('msg', [`Hi! Welcome to the HypeZone! You're among the closest on Mixer to killing the people with your guns. Everyone is here to cheer you on in the pursuit of your killing spree. Have fun or whatever.`]);
                    }
                    else if (~chat.indexOf('test')) {
                        socket.call('msg', [`Bot is working here!`]);
                    }
                } catch (e) {
                    ac.error(`Admin error: \n${e}`);
                }

            }

            /*
            CUSTOM COMMANDS
             */
            else if (superUser && cmd === 'command') {
                custom = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/commands.json')));
                try {
                    if(chat.startsWith('command add')){
                        var rank, commandName, commandMsg;
                        chat = chat.replace('command add', '');
                        commandName = chat.split(' ')[1];
                        chat = chat.replace(commandName, '');
                        if(~chat.indexOf("(rank:")){
                            switch(chat){
                                case ~chat.indexOf('(rank:everyone)'):
                                    rank = "everyone";
                                    break;
                                case ~chat.indexOf('(rank:mod)'):
                                    rank = "mod";
                                    break;
                                default:
                                    rank = "everyone";

                            }
                        }else{
                            rank = "everyone";
                        }
                        commandMsg = chat.trim();
                        commandMsg = commandMsg.replace(`(rank:${rank})`, "");
                        say(`Command ${commandName}, ${commandMsg}. Has been added`);
                        custom.commands.cmds.push({name: commandName, message: commandMsg, count: 0, rank: rank});
                        fs.writeFileSync(path.join(__dirname,'../../text/commands.json'), JSON.stringify(custom,null,2));
                        custom = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/commands.json')));
                    }
                    else if(chat.startsWith('command remove')){
                        var remove, pos = 0, removed = false;
                        chat = chat.replace('command remove', '');
                        remove = chat.split(' ')[1];
                        custom.commands.cmds.some(item=>{
                            if(item.name === remove){
                                custom.commands.cmds.splice(pos,1);
                                fs.writeFileSync(path.join(__dirname,'../../text/commands.json'), JSON.stringify(custom,null,2));
                                custom = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/commands.json')));
                                say(`Command ${remove}, was removed.`);
                                return removed = true;
                            }
                            pos++;
                        });
                        if(!removed)
                            say(`Command ${remove}, wasn't a command.`);
                    }
                } catch (e) {
                    ac.error(`Error at custom commands, send the following to Unsmart: \n${e}`)
                }

            }

            /*
             * STREAM SETTINGS
             */
            //Set Title
            else if (cmd === ('status') && superUser) {
                msg = chat.split(' ');
                let title = '';
                msg.forEach(function (item) {
                    title += item.toString() + ' ';
                });
                title = title.trim();
                if (!!title) {
                    util.titleSet(title);
                } else
                    say(`@${user} a title must be given.`);
            }

            //Set Game
            else if (cmd === ('setgame') && superUser) {
                msg = chat.split(' ');
                let game = '';
                msg.forEach(function (item) {
                    game += item.toString() + ' ';
                });
                if (!!game) {
                    util.gameSet(game.trim());
                } else
                    say(`@${user} a game must be given. [Ex. Rocket League]`);
            }

            //Set Rating
            else if (cmd === (`setrating`) && superUser) {
                msg = chat.split(' ');
                let rating = msg[0].toLowerCase();
                if (!!rating && rating === ("teen" || "family" || "18+")) {
                    util.ratingSet(rating);
                } else{
                    reply = `@${user} a rating must be given. [Teen | Family | 18+]`;
                    say(reply);
                }
            }

            /*
            MOD COMMANDS
             */
            else if (cmd === (' tttttt') && superUser) {

            }

            /*
             * GENERAL COMMANDS
             */

            //Get Uptime
            else if (cmd === (`uptime`)) {
                util.isLive().then(function (live) {
                    reply = live ? replySettings.streamTime.onlineMessage : replySettings.streamTime.offlineMessage;
                    reply = replace(reply);

                    if (live) {
                        util.getStreamTime().then(function (time) {
                            reply = reply.replace('(_uptime_)', time);
                            say(reply);
                        });
                    } else {
                        say(reply);
                    }
                    ac.info([`Uptime call by: ${user}`]);
                });


            }


            else if(cmd === "hours"){
                util.getHours(user).then(()=>{
                    util.getFile('users').then(file=>{
                        var hours = file.users[user].hours;
                        reply = `You have logged ${hours} hours in the channel!`;
                        say(reply);
                        ac.info([`Told ${user} his hours: ${hours}`]);
                    });
                });
                ac.info([`Hours call by: ${user}`]);
            }


            else if (cmd === ('test')) {
                reply = `@${user} im still alive.`;
                say(reply);
                ac.info([`Test call by: ${user}`]);
            }


            /*
             * QUEUE SYSTEM
             */

            // TURN QUEUE ON
            else if (cmd === ('queue') && superUser) {

                queueOn = !queueOn;
                reply = `@${user} the queue has been set ` + queueOn ? 'on': 'off';
                say(reply);
                queueF.queue.on = queueOn;
                fs.writeFileSync(path.join(__dirname,'../../text/queue.json'), JSON.stringify(queueF, null, 2));
            }

            //QUEUE COMMANDS

            //JOIN QUEUE
            else if (cmd === ('join') && queueOn) {
                q.enqueue(user).then(rep =>{
                    say(rep);
                });
                ac.info(`Join call by: ${user}`);
            }

            //NEXT IN QUEUE
            else if (cmd === (`next`) && superUser) {
                let amount = chat.split(' ')[1];
                if(!!amount && amount > 1)
                    q.upAmount(amount).then(rep=>{say(rep);});
                else
                    q.next().then(rep=>{say(rep);});
                ac.info(`${user} next in queue call`);

            }

            //RESET QUEUE
            else if (cmd === (`reset`) && superUser) {
                q.reset();
                socket.call('msg', [`@${data.user_name} the queue has been reset!`]);
            }


            /*
             * CHECK IF CUSTOM COMMAND
             */

            else {
                custom = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/commands.json')));
                var customc = false;
                custom.commands.cmds.some(function (item) {
                   if(cmd === item.name){
                       reply = item.message;
                       reply = reply.replace('(_user_)', user).replace('(_target_)', target === null ? user : target);
                       say(reply);
                       item.count = item.count + 1;
                       fs.writeFileSync(path.join(__dirname,'../../text/commands.json'), JSON.stringify(custom,null,2));
                       return customc = true;
                   }
                });

                //UNKNOWN COMMAND
                if(!customc){
                    if (!superUser)
                        socket.call('deleteMessage', [data.id]);
                    //socket.call('msg', [`@${data.user_name} command, '${cmd}', is unknown.`]);
                }

            }
        }

        /*
         * CHAT MESSAGES
         */

        else {
            util.getFile('users').then(async main=>{
                let fullChat = '';
                data.message.message.forEach(function (msgPart) {
                    fullChat += msgPart.text;
                });
                fullChat.trim();

                let msg =`${user}: ${fullChat}`;

                if(~data.user_roles.toString().toLowerCase().indexOf("owner")){
                    ac.appendChat(user,data.message.message, "red");
                }else if(~data.user_roles.toString().toLowerCase().indexOf("mod")){
                    ac.appendChat(user,data.message.message, "green");
                }else if(~data.user_roles.toString().toLowerCase().indexOf("pro")){
                    ac.appendChat(user,data.message.message, "purple");
                }else{
                    ac.appendChat(user,data.message.message, "blue");
                }

                ac.info(msg);
                chatLogs.Logs.push({id: data.id, msg: fullChat, user: user});
                fs.writeFileSync(path.join(__dirname,'../../text/chatLogs.json'), JSON.stringify(chatLogs, null, 2));
                if (typeof main.users[user] === "undefined") {
                    util.newUser(user).then(()=>{
                        util.getFile('users').then(file=>{
                            file.users[user].lastMsg = fullChat;
                            fs.writeFileSync(path.join(__dirname,'../../text/users.json'), JSON.stringify(file, null, 2));
                        });
                    });

                } else {
                    main.users[user].lastMsg = fullChat;
                    fs.writeFileSync(path.join(__dirname,'../../text/users.json'), JSON.stringify(main, null, 2));
                }
            });


        }

    });
    var dm = function (user,message) {
        socket.call('whisper', [user,message]);
    };

    say = (message)=>{
        socket.call('msg', [message]).catch(function (error) {
            setTimeout(retry, 200);
            console.error(error);
        });

        function retry(){
            say(message);
        }

        //testing(message);
    };

    var testing = (message)=>{
        dm('iamEklo', message);
        setTimeout(f,3000);
        function f() {
            dm('unsmart', message);
        }
    };

    async function replace(str){
        return new Promise(resolve => {
            str = str.replace('(_user_)', user);
            str = str.replace('(_target_)', target === null ? user : target);
            if(~str.indexOf('(_game_)')){
                util.getGame().then(game =>{
                    str = str.replace(`(_game_)`, game);
                });
            }
            if(~str.indexOf('(_uptime_)')){
                util.getStreamTime().then(streamTime =>{
                    str = str.replace('(_uptime_)', streamTime);
                });
            }
            if(~str.indexOf('(_')){
                ac.info(str);
            }
            resolve(str);
        });

    }

    socket.on('error', () => {
        ac.error('Socket error trying to reconnect.');
        tryCount--;
        if(tryCount <= 0){
            ac.error('Couldn\'t connect to socket.')
        }else{
            setTimeout(function (){cs(userId, channelId, endpoints, authkey);}, 3000);
        }

    });

    return socket.auth(channelId, userId, authkey).then(() => {
        ac.log('Chat connection successfully established.');
        say('Bot has joined!');
    });
}

module.exports.createChatSocket = function(userId, channelId, endpoints, authkey){
        return cs(userId, channelId, endpoints, authkey);
};

module.exports.chat = function (msg) {
    return say(msg);
};


var collection;
function queue(){
    this.get = async function () {
        return new Promise(resolve => {
            getQueue().then(file=>{
                collection = file.queue.list;
                resolve(collection);
            });
        });

    };
    this.enqueue = async function (user) {
        return new Promise(resolve => {
            var pos = 1, already = false;
            getQueue().then(file=>{
                collection = file.queue.list;
                collection.some(userQ=>{
                    if(userQ === user){
                        already = true;
                        resolve(`${user} you are already in the queue at position ${pos}`);
                    }
                    pos++;
                });
                if(!already){
                    collection.push(user);
                    fs.writeFileSync(path.join(__dirname,'../../text/queue.json'), JSON.stringify(file,null,2));
                    resolve(`${user} you have been added to the queue at position ${pos}`);
                }

            });
        });


    };
    this.remove = async function (user) {
        return new Promise(resolve => {
            getQueue().then(file=>{
                var pos = 0;
                collection.some(userQ =>{
                    collection = file.queue.list;
                    if(user === userQ){
                        collection.splice(pos,1);
                        fs.writeFileSync(path.join(__dirname,'../../text/queue.json'), JSON.stringify(queue,null,2));
                        resolve(true);
                    }
                    pos++;
                });
                resolve(false);
            });
        });


    };
    this.next = async function () {
        return new Promise(resolve => {
            this.isEmpty().then(empty=>{
                if(empty)
                    resolve(`Queue is empty`);
                else {
                    getQueue().then(file=>{
                        collection = file.queue.list;
                        var next = collection[0];
                        collection.splice(0,1);
                        fs.writeFileSync(path.join(__dirname,'../../text/queue.json'), JSON.stringify(queue,null,2));
                        resolve(`Next person in queue => ${next}`);
                    });
                }
            });

        });


    };
    this.upAmount = async function (amount) {
        return new Promise(resolve => {
            var nextUp = '';

            getQueue().then(file=>{
                collection = file.queue.list;
                if(collection.length === 1){
                    this.next();
                }else{
                    for(let i = 0; i < amount;){
                        let x = i++;
                        if(x === amount || x === collection.length()){
                            nextUp += 'and, ' + collection[x];
                        }else{
                            nextUp += collection[x] + ', ';
                        }

                    }
                }

            }).then(()=>{
                resolve(nextUp);
            });
        });

    };
    this.isEmpty = async function () {
        return new Promise(resolve => {
            getQueue().then(file=>{
                collection = file.queue.list;
                resolve(collection.length === 0);
            });
        });



    };
    this.reset = async function () {
        return new Promise(resolve => {
            getQueue().then(file=>{
                collection = file.queue.list;
                collection.splice(0,collection.length);
                fs.writeFileSync(path.join(__dirname,'../../text/queue.json'), JSON.stringify(file,null,2));
                resolve(true);
            });
        });


    };
}
var q = new queue();

async function getQueue() {
    return new Promise(async resolve => {
        util.getFile('queue').then(file=>{
            resolve(file);
        });
    });

}
