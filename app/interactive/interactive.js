const interactive = require('beam-interactive-node2');
const ws = require('ws');
const fs = require("fs");
const path = require("path");

const client = new interactive.GameClient();

const {IButton, IParticipant, setWebSocket} = interactive;
setWebSocket(ws);


start();

function start(){
    var config = JSON.parse(fs.readFileSync(path.join(__dirname,'../../text/authKeys.json'), 'utf-8'));
    var tokenStreamer = config.streamer;
    var int = JSON.parse(fs.readFileSync(path.join(__dirname, '../../text/interactive.json'), 'utf-8'));
    //Open interactive game client
    client.open({
        authToken: tokenStreamer,
        versionId: int.version
    }).then(()=>{
        client.synchronizeState().then(([groups,scenes]) => {
            var scene1 = scenes[0];
            scene1.controls.forEach(control =>{
                //Textbox no submit??
                control.on('change', ((inputEvent, participant) => {
                    console.log(
                        `${participant.username} changed ${
                            inputEvent.input.controlID
                            } with the value: ${inputEvent.input.value}`,
                    );

                    if (inputEvent.transactionID) {
                        // client.captureTransaction(inputEvent.transactionID).then(() => {
                        //     console.log(`Charged ${participant.username} ${control.cost} sparks!`);
                        // });
                        console.log(control.cost + " is the cost");
                    }
                }));


                //Textbox submit button
                control.on('submit', (inputEvent, participant) => {
                    console.log(
                        `${participant.username} submit ${
                            inputEvent.input.controlID
                            } with the value: ${inputEvent.input.value}`
                    );

                    console.log(control.cost);


                    if (inputEvent.transactionID) {
                        // client.captureTransaction(inputEvent.transactionID).then(() => {
                        //      console.log(`Charged ${participant.username} ${control.cost} sparks!`);
                        // });
                        console.log(`Charged ${participant.username} ${control.cost} sparks!`);
                    }
                });


                //Basic button
                control.on('mousedown', (inputEvent, participant) => {
                    console.log(`${participant.username} pushed, ${inputEvent.input.controlID}`);

                    if (inputEvent.transactionID) {
                        // client.captureTransaction(inputEvent.transactionID)
                        //     .then(() => {
                        //         console.log(`Charged ${participant.username} ${control.cost} sparks!`);
                        //     });
                        console.log(`Charged ${participant.username} ${control.cost} sparks!`);
                    }

                });

            });
        });
        console.log("Connected to interactive successfully");
        //Tell the client your ready (show the controls)
        return client.ready(true);
    });

}

function stop(){
    client.close();
}

module.exports.start = () =>{
    start();
};
module.exports.stop = () =>{
  stop();
};



client.state.on('participantJoin', participant => {
    console.log(`${participant.username} Joined`);
});
client.state.on(
    'participantLeave',
    (participantSessionID, participant) => {
        console.log(`${participant.username} Left`);
    }
);


