"use strict";

const Carina = require('carina').Carina, ws = require('ws'), fs = require('fs'), path = require('path'), cs = require(path.join(__dirname,'chat/chatSocket')), ac = require(path.join(__dirname,'accounts')), utils = require(path.join(__dirname,'Utils'));
Carina.WebSocket = ws;
const ca = new Carina({isBot: true}).open();
let followed, following, sesh;
module.exports = {
    update: function (channelId) {
        try{
            ca.subscribe(`channel:${channelId}:update`, data => {
                if(JSON.stringify(data).indexOf('online') > -1){
                    utils.getViewers();
                    ac.info('Getting viewers [online update]');
                }else{
                    //ac.info('Update event ' + JSON.stringify(data));
                }
            });
        }catch (e){
            ac.error(`Error at update event, send following data to unsmart: \n${e}`);
        }

    },
    follow: function(channelId){
        try{
            ac.info("Subscribing to follow events");
            ca.subscribe(`channel:${channelId}:followed`, async data => {
                followed = data.user.username;
                if (data.following) {
                    utils.getFile('users').then(main=>{
                        utils.getFile('events').then(file=>{
                            if (main.users.hasOwnProperty(followed)) {
                                following = main.users[followed].following;
                                sesh = file.sessionEvents.follows;
                                if (!following) {
                                    following = main.users[followed].following = true;
                                    sesh.push(followed);
                                    fs.writeFileSync(path.join(__dirname,'../text/events.json'), JSON.stringify(file, null, 2));
                                    fs.writeFileSync(path.join(__dirname,'../text/users.json'), JSON.stringify(main, null, 2));
                                    cs.chat(`Thanks @${followed} for the follow! I hope you enjoy your stay!`);
                                    ac.info(`User followed: ${followed}`);
                                }
                            } else {
                                ac.info(`Creating user ${followed} ----Follow Event`);
                                utils.newUser(followed).then(() =>{
                                    ac.info(`Done creating user ${followed} ----Follow Event`);
                                    following = main.users[followed].following = true;
                                    sesh.push(followed);
                                    fs.writeFileSync(path.join(__dirname,'../text/events.json'), JSON.stringify(file, null, 2));
                                    fs.writeFileSync(path.join(__dirname,'../text/users.json'), JSON.stringify(main, null, 2));
                                    cs.chat(`Thanks @${followed} for the follow! I hope you enjoy your stay!`);
                                    ac.info(`User followed: ${followed}`);
                                });


                            }
                        });
                    });

                }
            });
        }catch (e){
            ac.error(`Error at follow event, send following data to unsmart: \n${e}`);
        }

    },
    sub: function (channelId) {
        try{
            ac.info("Subscribing to subscription events");
            ca.subscribe(`channel:${channelId}:subscribed`, async data => {
                cs.chat(`${data.user.username} has just subscribed!`);
            });
            ca.subscribe(`channel:${channelId}:resubShared`, async data => {
                cs.chat(`${data.user.username} has been subbed for ${data.totalMonths} now!`);
            });
        }catch (e){

        }

    },
    host: function (channelId) {
        try{
            let hosted = false;
            ac.info("Subscribing to host events");
            ca.subscribe(`channel:${channelId}:hosted`, async data => {

                ac.createFiles().then(()=>{
                    utils.getFile('events').then(file=>{
                        file.sessionEvents.hosts.some(x => {
                            if( x === data.hoster.token){
                                return hosted = true;
                            }
                        });
                        if(!hosted){
                            cs.chat(`${data.hoster.token} has just hosted you and brought ${data.hoster.viewersCurrent} viewers!`);
                            file.sessionEvents.hosts.push(data.hoster.token);
                            fs.writeFileSync(path.join(__dirname,'../text/events.json'), JSON.stringify(file, null, 2));
                        }
                    });

                });



            });
        }catch (e){
            ac.error(`Error at host event, send following data to unsmart: \n${e}`);
        }

    }
};

