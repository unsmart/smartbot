const path = require('path'), fs = require('fs');
const url = require('url');
const electronOauth2 = require('electron-oauth2');
let key = require(path.join(__dirname, 'text/authKeys.json'));

const {app, BrowserWindow, Menu} = require('electron');

let mainWindow;


// Listen for app to be ready
app.on('ready', () => {
    createMainWindow();
    if(!(!!key.streamer)){
        getAuth();
    }else{
        refreshAuth();
    }
});

var config = {
    clientId: '4b52a49602144e68f5270393193e1aeee187ad520773a619',
    authorizationUrl: 'https://mixer.com/oauth/authorize',
    tokenUrl: 'https://mixer.com/api/v1/oauth/token',
    redirectUri: 'http://totalpvp.us',
    useBasicAuthorizationHeader: false
};
const windowParams = {
    alwaysOnTop: true,
    autoHideMenuBar: true,
    webPreferences: {
        nodeIntegration: true
    }
};

const options = {
    scope: 'chat:chat+chat:bypass_catbot+chat:bypass_filter+chat:bypass_links+chat:bypass_links+chat:bypass_links+chat:bypass_links+chat:bypass_links+chat:bypass_links+chat:connect+chat:edit_options+chat:giveaway_start+chat:poll_start+chat:poll_vote+chat:purge+chat:remove_message+chat:timeout+chat:view_deleted+chat:whisper+interactive:manage:self+interactive:robot:self+channel:update:self+channel:partnership:self+channel:details:self'
};
const myApiOauth = electronOauth2(config, windowParams);
// Handle add item window
function getAuth(){
    myApiOauth.getAccessToken(options)
        .then(token => {
            key.streamer = token.access_token;
            fs.writeFileSync(path.join(__dirname,'text/authKeys.json'), JSON.stringify(key,null,2));
            //console.log('Write auth key ' + token.access_token + '?');
            mainWindow.loadURL(`file://${__dirname}/index.html`);
        }).catch(err =>{
            console.log(err);
    });
}

function refreshAuth(){
    myApiOauth.refreshToken(options)
        .then(token => {
            if(token.error !== null){
                getAuth();
            }else{
                key.streamer = token;
                fs.writeFileSync(path.join(__dirname,'text/authKeys.json'), JSON.stringify(key,null,2));
                //console.log('Write auth key ' + token + '? refresh!');
                mainWindow.loadURL(`file://${__dirname}/index.html`);
            }

        }).catch(err =>{
        console.log(err);
    });
}

function createMainWindow(){
    // Create new window
    mainWindow = new BrowserWindow({});
    // Quit app when closed
    mainWindow.on('closed', function(){
        app.quit();
    });

    // Insert menu
    mainWindow.webContents.openDevTools();
    Menu.setApplicationMenu(null);
}


